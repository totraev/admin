import mongoose from 'mongoose';

var HOST = process.env.MONGO_OWM_URL ||
          'mongodb://localhost:27018/owm_members_prod';

var connection = mongoose.createConnection(HOST);

connection.on('error', function(err) {
  console.error('MongoDB connection error: ' + err);
  process.exit(-1);
});

module.exports = connection;
