'use strict';

import paypal from 'paypal-rest-sdk';

export function index(req, res){

  var listPayment = {
    count: req.query.count || 20,
  };

  if(req.query.start_id) listPayment.start_id = req.query.start_id;
  if(req.query.dateFrom) listPayment.start_time = req.query.dateFrom + 'T23:59:59Z';
  if(req.query.dateTo) listPayment.end_time = req.query.dateTo + 'T00:00:00Z';
  if(req.query.sort_by) listPayment.sort_by = req.query.sort_by;
  if(req.query.sort_order) listPayment.sort_order = req.query.sort_order;

  console.log(req.query);

  paypal.payment.list(listPayment, function (error, payment) {
      if (error) {
          res.status(200).json(error)
      } else {
          res.status(200).json(payment);
      }
  });
}

export function show(req, res) {
  paypal.payment.get(req.params.id, function (error, payment) {
    if (error) {
        res.status(200).json(error)
    } else {
        res.status(200).json(payment);
    }
  });
}
