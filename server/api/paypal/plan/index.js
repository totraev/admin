'use strict';

import {Router} from 'express';
import * as controller from './plan.controller';
import * as auth from '../../../auth/auth.service';

var router = new Router();

router.get('/', controller.index);
router.get('/:id', controller.show);

export default router;
