import paypal from 'paypal-rest-sdk';

export function index(req, res){
  var list_billing_plan = {
      status: req.query.status || 'ACTIVE',
      page_size: req.query.page_size || 20,
      page: req.query.page || 0,
      total_required: 'yes'
  };

  paypal.billingPlan.list(list_billing_plan, function (error, billingPlan) {
    if (error){
      res.status(200).json(error);
    } else {
      res.status(200).json(billingPlan);
    }
  });
}

export function show(req, res){
  paypal.billingPlan.get(req.params.id, function (error, billingPlan) {
    if (error) {
        res.status(200).json(error);
    } else {
        res.status(200).json(billingPlan);
    }
  });
}
