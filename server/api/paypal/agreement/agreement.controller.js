'use strict';

import _ from 'lodash';
import paypal from 'paypal-rest-sdk';
import Subscription from '../../admin/subscription/subscription.model';


export function show(req, res) {
  paypal.billingAgreement.get(req.params.id, function (error, billingAgreement) {
    if (error) {
        res.status(200).json(error);
    } else {
        res.status(200).json(billingAgreement);
    }
  });
}

export function suspend(req, res) {
  var suspend_note = {
    "note": "Suspending the agreement"
  };

  paypal.billingAgreement.suspend(req.params.id, suspend_note, function (error, response) {
      if (error) {
          console.log(error);
          res.status(200).json(error);
      } else {
          console.log("Suspend Billing Agreement Response");
          console.log(response);
          res.status(200).json(response);
      }
  });
}

export function cancel(req, res) {
  var cancel_note = {
    "note": "Canceling the agreement"
  };

  paypal.billingAgreement.cancel(req.params.id, cancel_note, function (error, response) {
      if (error) {
          res.status(200).json(error);
      } else {
          res.status(200).json(response);
      }
  });
}

export function reactivate(req, res) {
  var reactivate_note = {
    "note": "Reactivate the agreement"
  };

  paypal.billingAgreement.reactivate(req.params.id, reactivate_note, function (error, response) {
    if (error) {
        res.status(200).json(error);
    } else {

        Subscription.find({pp_agreement_id: req.params.id})
          .update({status: 'ACTIVATED', active: true})
          .execAsync()
          .then((res)=>{console.log(res)});

        res.status(200).json(response);
    }
  });
}
