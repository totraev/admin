'use strict';

var express = require('express');
var controller = require('./agreement.controller');

var router = express.Router();

router.get('/:id', controller.show);
router.post('/:id/suspend', controller.suspend);
router.post('/:id/cancel', controller.cancel);
router.post('/:id/re-activate', controller.reactivate);

module.exports = router;
