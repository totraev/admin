'use strict';

var mongoose = require('mongoose');
var connection = require('../../../owm.mmbrs');

var PaymentSchema = new mongoose.Schema({
  type: String,
  date: String,
  amount: String,
  service_name: String,
  plan_desc: String,

  pp_txn_id: String,
  pp_agreement_id: String
});

export default connection.model('Payment', PaymentSchema, 'payments');
