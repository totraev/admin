'use strict';

var mongoose = require('mongoose');
var connection = require('../../../owm.mmbrs');

var NotificationSchema = new mongoose.Schema({
  txn_type: String,
  ipn_msg: {}
});

export default connection.model('Notification', NotificationSchema, 'notifications');
