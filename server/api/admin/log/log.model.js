'use strict';

var mongoose = require('mongoose');
var connection = require('../../../owm.mmbrs');

var LogSchema = new mongoose.Schema({
  service: String,
  service_key: String,
  action: String,  
});

export default connection.model('Log', LogSchema, 'logs');
