'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var logCtrlStub = {
  index: 'logCtrl.index',
  show: 'logCtrl.show',
  create: 'logCtrl.create',
  update: 'logCtrl.update',
  destroy: 'logCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var logIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './log.controller': logCtrlStub
});

describe('Log API Router:', function() {

  it('should return an express router instance', function() {
    expect(logIndex).to.equal(routerStub);
  });

  describe('GET /api/logs', function() {

    it('should route to log.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'logCtrl.index')
        ).to.have.been.calledOnce;
    });

  });

  describe('GET /api/logs/:id', function() {

    it('should route to log.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'logCtrl.show')
        ).to.have.been.calledOnce;
    });

  });

  describe('POST /api/logs', function() {

    it('should route to log.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'logCtrl.create')
        ).to.have.been.calledOnce;
    });

  });

  describe('PUT /api/logs/:id', function() {

    it('should route to log.controller.update', function() {
      expect(routerStub.put
        .withArgs('/:id', 'logCtrl.update')
        ).to.have.been.calledOnce;
    });

  });

  describe('PATCH /api/logs/:id', function() {

    it('should route to log.controller.update', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'logCtrl.update')
        ).to.have.been.calledOnce;
    });

  });

  describe('DELETE /api/logs/:id', function() {

    it('should route to log.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'logCtrl.destroy')
        ).to.have.been.calledOnce;
    });

  });

});
