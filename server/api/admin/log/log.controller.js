/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/logs              ->  index
 * POST    /api/logs              ->  create
 * GET     /api/logs/:id          ->  show
 * PUT     /api/logs/:id          ->  update
 * DELETE  /api/logs/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import Log from './log.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.saveAsync()
      .spread(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.removeAsync()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Logs
export function index(req, res) {
  var skip = parseInt(req.query.skip, 10) || 0;
  var limit = parseInt(req.query.limit, 10) || 1000;
  var query = {};

  if(req.query.service) query.service = req.query.service;
  if(req.query.plan) query.service_key = req.query.plan;
  if(req.query.action) query.action = req.query.action;

  if(req.query.dateFrom || req.query.dateTo) query.created_at = {};
  if(req.query.dateTo) query.created_at.$lt = new Date(req.query.dateTo + 'T23:59:59.999Z');
  if(req.query.dateFrom) query.created_at.$gte = new Date(req.query.dateFrom + 'T00:00:00.000Z');

  console.log(Date(req.query.dateTo + 'T23:59:59.999Z'));

  Log.find(query)
    .skip(skip)
    .limit(limit)
    .execAsync()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Log from the DB
export function show(req, res) {
  Log.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Log in the DB
export function create(req, res) {
  Log.createAsync(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing Log in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Log.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Log from the DB
export function destroy(req, res) {
  Log.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
