/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/subscriptions              ->  index
 * POST    /api/subscriptions              ->  create
 * GET     /api/subscriptions/:id          ->  show
 * PUT     /api/subscriptions/:id          ->  update
 * DELETE  /api/subscriptions/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import Subscription from './subscription.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.saveAsync()
      .spread(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.removeAsync()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Subscriptions
export function index(req, res) {
  var skip = parseInt(req.query.skip, 10) || 0;
  var limit = parseInt(req.query.limit, 10) || 1000;
  var query = {};

  if(req.query.service) query.service_key = req.query.service;
  if(req.query.plan) query.plan_key = req.query.plan;
  if(req.query.status) query.status = req.query.status;
  if(req.query.state) query.active = req.query.state;
  console.log(query)

  Subscription.find(query)
    .skip(skip)
    .limit(limit)
    .execAsync()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Subscription from the DB
export function show(req, res) {
  Subscription.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Subscription in the DB
export function create(req, res) {
  Subscription.createAsync(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing Subscription in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Subscription.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Subscription from the DB
export function destroy(req, res) {
  Subscription.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
