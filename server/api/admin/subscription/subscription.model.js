'use strict';

var mongoose = require('mongoose');
var connection = require('../../../owm.mmbrs');

var SubscriptionSchema = new mongoose.Schema({
  service_key: String,
  plan_key: String,
  status: String,
  active: Boolean,

  // Paypal data
  create_date: String,
  cancel_date: String,
  pp_token: String,
  pp_agreement_id: String,
  pp_plan_id: String
});

export default connection.model('Subscription', SubscriptionSchema, 'subscriptions');
