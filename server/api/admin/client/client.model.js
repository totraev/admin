'use strict';
var mongoose = require('mongoose');
var connection = require('../../../owm.mmbrs');

var ClientSchema = new mongoose.Schema({
  email: String,
  remember_created_at: Date,

  // Trackable
  sign_in_count: Number,
  current_sign_in_at: Date,
  last_sign_in_at: Date,
  current_sign_in_ip: String,
  last_sign_in_ip: String,

  // Custom fields
  name: String,
  appid: String,
  role_id: Number,
  subscription_flag: Number,

  // Billing
  active: String,
  services: {},
  paypal_user: Boolean
});

export default connection.model('Client', ClientSchema, 'users');
