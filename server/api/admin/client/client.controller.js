/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/clients              ->  index
 * POST    /api/clients              ->  create
 * GET     /api/clients/:id          ->  show
 * PUT     /api/clients/:id          ->  update
 * DELETE  /api/clients/:id          ->  destroy
 */

'use strict';
import mongoose from 'mongoose'
import _ from 'lodash';

import Client from './client.model';
import Subscription from '../subscription/subscription.model';
import Payment from '../payment/payment.model';
import Log from '../log/log.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.saveAsync()
      .spread(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.removeAsync()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Clients
export function index(req, res) {
  var skip = parseInt(req.query.skip, 10) || 0;
  var limit = parseInt(req.query.limit, 10) || 1000;
  var query = {};

  if(req.query.name)
    query.name = { $regex: new RegExp(req.query.name, 'i')}
  if(req.query.email)
    query.email = { $regex: new RegExp(req.query.email, 'i')}
  if(req.query.appid)
    query.appid = { $regex: new RegExp(req.query.appid, 'i')}
  if(typeof req.query.paypal_user !== 'undefined')
    query.paypal_user = req.query.paypal_user === 'true' ? true : { $ne: true };

  Client.find(query)
    .skip(skip)
    .limit(limit)
    .execAsync()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Client from the DB
export function show(req, res) {
  Client.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Client in the DB
export function create(req, res) {
  Client.createAsync(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing Client in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Client.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Client from the DB
export function destroy(req, res) {
  Client.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}

// Subscriptions for given user
// GET /:id/subscriptions
export function subscriptions(req, res){
  var skip = parseInt(req.query.skip, 10) || 0;
  var limit = parseInt(req.query.limit, 10) || 1000;
  var id = new mongoose.Types.ObjectId(req.params.id);

  Subscription.find({user_id: id})
    .skip(skip)
    .limit(limit)
    .execAsync()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Payments for given user
// GET /:id/payments
export function payments(req, res){
  var skip = parseInt(req.query.skip, 10) || 0;
  var limit = parseInt(req.query.limit, 10) || 1000;
  var id = new mongoose.Types.ObjectId(req.params.id);

  Payment.find({user_id: id})
    .skip(skip)
    .limit(limit)
    .execAsync()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Logs for given user
// GET /:id/logs
export function logs(req, res){
  var skip = parseInt(req.query.skip, 10) || 0;
  var limit = parseInt(req.query.limit, 10) || 1000;
  var id = new mongoose.Types.ObjectId(req.params.id);

  Log.find({user_id: id})
    .skip(skip)
    .limit(limit)
    .execAsync()
    .then(respondWithResult(res))
    .catch(handleError(res));
}
