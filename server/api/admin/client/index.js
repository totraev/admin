'use strict';

var express = require('express');
var controller = require('./client.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

router.get('/:id/subscriptions', controller.subscriptions);
router.get('/:id/payments', controller.payments);
router.get('/:id/logs', controller.logs);

module.exports = router;
