/**
 * Main application routes
 */

'use strict';

import errors from './components/errors';
import path from 'path';

export default function(app) {
  // Insert routes below
  app.use('/paypal/payments', require('./api/paypal/payment'));
  app.use('/paypal/agreements', require('./api/paypal/agreement'));
  app.use('/paypal/plans', require('./api/paypal/plan'));
  app.use('/api/notifications', require('./api/admin/notification'));
  app.use('/api/logs', require('./api/admin/log'));
  app.use('/api/payments', require('./api/admin/payment'));
  app.use('/api/subscriptions', require('./api/admin/subscription'));
  app.use('/api/clients', require('./api/admin/client'));
  app.use('/api/things', require('./api/admin/thing'));
  app.use('/api/users', require('./api/admin/user'));

  app.use('/auth', require('./auth'));

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
   .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get((req, res) => {
      res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
    });
}
