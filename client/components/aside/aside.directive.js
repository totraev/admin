'use strict';

angular.module('owmAdminApp')
  .directive('aside', function() {
    return {
      templateUrl: 'components/aside/aside.html',
      restrict: 'E',
      link: function(scope, element) {
        element.addClass('aside');
      }
    };
  });
