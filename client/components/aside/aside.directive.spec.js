'use strict';

describe('Directive: aside', function () {

  // load the directive's module and view
  beforeEach(module('testApp'));
  beforeEach(module('components/aside/aside.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<aside></aside>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).to.equal('this is the aside directive');
  }));
});
