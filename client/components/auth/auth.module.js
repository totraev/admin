'use strict';

angular.module('owmAdminApp.auth', [
  'owmAdminApp.constants',
  'owmAdminApp.util',
  'ngCookies',
  'ui.router'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
