'use strict';

(function() {

angular.module('owmAdminApp.auth')
  .run(function($rootScope, $state, Auth) {
    // Redirect to login if route requires auth and the user is not logged in, or doesn't have required role
    $rootScope.$on('$stateChangeStart', function(event, next) {

      if (!next.data || !next.data.auth) {
        return;
      }

      if (typeof next.data.auth === 'string') {
        Auth.hasRole(next.data.auth, _.noop).then(has => {
          if (has) {
            return;
          }

          event.preventDefault();
          return Auth.isLoggedIn(_.noop).then(is => {
            $state.go(is ? 'account.login' : 'account.login');
          });
        });
      } else {
        Auth.isLoggedIn(_.noop).then(is => {
          if (is) {
            return;
          }

          event.preventDefault();          
          $state.go('account.login');
        });
      }
    });
  });

})();
