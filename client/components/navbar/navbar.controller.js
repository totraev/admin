'use strict';

class NavbarController {
  toggleAside(){
    let body = angular.element('body');

    if(body.hasClass('sidebar-collapse')){
      body.removeClass('sidebar-collapse');
    } else {
      body.addClass('sidebar-collapse');
    }
  }
}

angular.module('owmAdminApp')
  .controller('NavbarController', NavbarController);
