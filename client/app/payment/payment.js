'use strict';

angular.module('owmAdminApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.payment', {
        url: '/payment',
        templateUrl: 'app/payment/payment.html',
        controller: 'PaymentCtrl',
        controllerAs: 'pmt'
      });
  });
