'use strict';

(function(){

class PaymentController{

  constructor($resource, $uibModal) {
    this.uibModal = $uibModal;

    this.Payment = $resource('/api/payments');
    this.payments = [];
    this.overlay = false;

    this.totalItems = 0;
    this.lastPage = 0;
    this.firstPage = 0;

    this.currentPage = 1;

    this.pageLimit = 50;
    this.maxSize = 10;
    this.parts = 5;

    this.startIndex = 0;
    this.lastIndex = this.pageLimit;

    this.service = 'all';
    this.plan = 'all';
    this.plans = ['Startup plan', 'Developer plan', 'Professional plan', 'Enterprise plan', 'Starter plan', 'Medium plan', 'Advanced plan'];

    this.services = {
      Weather: ['Startup plan', 'Developer plan', 'Professional plan', 'Enterprise plan'],
      History: ['Starter plan', 'Medium plan', 'Advanced plan']
    }

    this.getNextPayments();
  }

  getPayments(skip, limit){
    this.overlay = true;
    this.payments = [];
    var query = {};

    if(skip) query.skip = skip;
    if(limit) query.limit = limit;
    
    if(this.service != 'all') query.service = this.service;
    if(this.plan != 'all') query.plan = this.plan;

    this.Payment.query(query, (payments)=>{
      this.payments = payments;

      this.totalItems = skip + this.payments.length;
      // Add fake items if not last pages
      this.totalItems += this.payments.length < limit ?  0 : this.pageLimit * this.maxSize;
      this.overlay = false;
    });
  }

  getNextPayments(){
    var skip = this.lastPage * this.pageLimit;
    var limit = this.parts * this.pageLimit * this.maxSize;

    this.getPayments(skip, limit);

    this.firstPage = this.lastPage + 1;
    this.lastPage = this.firstPage - 1 + this.parts * this.maxSize;
  }

  getPrevPayments(){
    var skip = (this.firstPage - 1) * this.pageLimit - this.parts*this.maxSize*this.pageLimit;
    var limit = this.parts * this.pageLimit * this.maxSize;

    this.getPayments(skip, limit);

    this.lastPage = this.firstPage - 1;
    this.firstPage = this.lastPage + 1 - this.parts * this.maxSize;
  }

  pageChanged(){
    if(this.currentPage > this.lastPage){
      this.getNextPayments();
    }
    if(this.currentPage < this.firstPage){
      this.getPrevPayments();
    }

    this.startIndex = (this.currentPage - 1)*this.pageLimit - (this.firstPage-1)*this.pageLimit;
    this.lastIndex = this.startIndex + this.pageLimit;
  }

  search(){
    var limit = this.parts * this.pageLimit * this.maxSize;
    this.plans = this.service == 'all'? _.concat(this.services.Weather, this.services.History): this.services[this.service];

    this.getPayments(0, limit);
  }
}

angular.module('owmAdminApp')
  .controller('PaymentCtrl', PaymentController);

})();
