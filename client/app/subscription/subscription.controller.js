'use strict';

(function(){

class SubscriptionController{

  constructor($resource, $uibModal) {
    this.uibModal = $uibModal;

    this.Subscription = $resource('/api/subscriptions');
    this.subscriptions = [];
    this.overlay = false;

    this.totalItems = 0;
    this.lastPage = 0;
    this.firstPage = 0;

    this.currentPage = 1;

    this.pageLimit = 50;
    this.maxSize = 10;
    this.parts = 5;

    this.startIndex = 0;
    this.lastIndex = this.pageLimit;

    this.service = 'all';
    this.plan = 'all';
    this.plans = ['startup', 'dev', 'pro', 'ent', 'start', 'med', 'adv'];
    this.status = 'all';
    this.state = 'all';

    this.services = {
      weather: ['startup', 'dev', 'pro', 'ent'],
      history: ['start', 'med', 'adv']
    }

    this.getNextSubscriptions();
  }

  getSubscriptions(skip, limit){
    this.overlay = true;
    this.subscriptions = [];
    var query = {};

    if(skip) query.skip = skip;
    if(limit) query.limit = limit;
    if(this.service != 'all') query.service = this.service;
    if(this.plan != 'all') query.plan = this.plan;
    if(this.status != 'all') query.status = this.status;
    if(this.state != 'all') query.state = this.state;

    this.Subscription.query(query, (subscriptions)=>{
      this.subscriptions = subscriptions;

      this.totalItems = skip + this.subscriptions.length;
      // Add fake items if not last pages
      this.totalItems += this.subscriptions.length < limit ?  0 : this.pageLimit * this.maxSize;
      this.overlay = false;
    });
  }

  getNextSubscriptions(){
    var skip = this.lastPage * this.pageLimit;
    var limit = this.parts * this.pageLimit * this.maxSize;

    this.getSubscriptions(skip, limit);

    this.firstPage = this.lastPage + 1;
    this.lastPage = this.firstPage - 1 + this.parts * this.maxSize;
  }

  getPrevSubscriptions(){
    var skip = (this.firstPage - 1) * this.pageLimit - this.parts*this.maxSize*this.pageLimit;
    var limit = this.parts * this.pageLimit * this.maxSize;

    this.getSubscriptions(skip, limit);

    this.lastPage = this.firstPage - 1;
    this.firstPage = this.lastPage + 1 - this.parts * this.maxSize;
  }

  pageChanged(){
    if(this.currentPage > this.lastPage){
      this.getNextSubscriptions();
    }
    if(this.currentPage < this.firstPage){
      this.getPrevSubscriptions();
    }

    this.startIndex = (this.currentPage - 1)*this.pageLimit - (this.firstPage-1)*this.pageLimit;
    this.lastIndex = this.startIndex + this.pageLimit;
  }

  search(){
    var limit = this.parts * this.pageLimit * this.maxSize;
    this.plans = this.service == 'all'? _.concat(this.services.weather, this.services.history): this.services[this.service];

    this.getSubscriptions(0, limit);
  }
}

angular.module('owmAdminApp')
  .controller('SubscriptionCtrl', SubscriptionController);

})();
