'use strict';

angular.module('owmAdminApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.subscription', {
        url: '/subscription',
        templateUrl: 'app/subscription/subscription.html',
        controller: 'SubscriptionCtrl',
        controllerAs: 'sub'
      });
  });
