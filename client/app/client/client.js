'use strict';

angular.module('owmAdminApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.client', {
        url: '/client/:id',
        templateUrl: 'app/client/client.html',
        controller: 'ClientCtrl',
        controllerAs: 'client'
      });
  });
