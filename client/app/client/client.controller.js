'use strict';

(function(){

class ClientController{

  constructor($stateParams, $resource){

    var User = $resource('/api/clients/:id', { id: $stateParams.id });

    User.get((user)=>{ this.user = user; });
  }

}

angular.module('owmAdminApp')
  .controller('ClientCtrl', ClientController);

})();
