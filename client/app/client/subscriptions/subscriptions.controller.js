'use strict';

(function(){

class ClientSubscriptionsController{

  constructor($stateParams, $resource, $uibModal) {
    this.uibModal = $uibModal;

    this.Subscriptions = $resource('/api/clients/:id/subscriptions', { id: $stateParams.id });
    this.Plan = $resource('/paypal/plans/:id');
    this.Agreement = $resource('/paypal/agreements/:id', { id: '@id' }, {
      suspend: { method: 'POST', url: '/paypal/agreements/:id/suspend' },
      reactivate: { method: 'POST', url: '/paypal/agreements/:id/re-activate' },
      cancel: { method: 'POST', url: '/paypal/agreements/:id/cancel' }
    });

    this.subscriptions = [];
    this.overlay = false;

    this.totalItems = 0;
    this.lastPage = 0;
    this.firstPage = 0;

    this.currentPage = 1;

    this.pageLimit = 20;
    this.maxSize = 5;
    this.parts = 5;

    this.startIndex = 0;
    this.lastIndex = this.pageLimit;
    this.jsonVisible = true;

    this.getNextSubscriptions();
  }

  getSubscriptions(skip, limit){
    this.overlay = true;
    this.subscriptions = [];

    this.Subscriptions.query({ skip: skip, limit: limit }, (subscriptions)=>{
      this.subscriptions = subscriptions;
      this.totalItems = skip + this.subscriptions.length;
      // Add fake items if not last pages
      this.totalItems += this.subscriptions.length < limit ?  0 : this.pageLimit * this.maxSize;
      this.overlay = false;
    });
  }

  getNextSubscriptions(){
    var skip = this.lastPage * this.pageLimit;
    var limit = this.parts * this.pageLimit * this.maxSize;

    this.getSubscriptions(skip, limit);

    this.firstPage = this.lastPage + 1;
    this.lastPage = this.firstPage - 1 + this.parts * this.maxSize;
  }

  getPrevSubscriptions(){
    var skip = (this.firstPage - 1) * this.pageLimit - this.parts*this.maxSize*this.pageLimit;
    var limit = this.parts * this.pageLimit * this.maxSize;

    this.getSubscriptions(skip, limit);

    this.lastPage = this.firstPage - 1;
    this.firstPage = this.lastPage + 1 - this.parts * this.maxSize;
  }

  pageChanged(){
    if(this.currentPage > this.lastPage){
      this.getNextSubscriptions();
    }
    if(this.currentPage < this.firstPage){
      this.getPrevSubscriptions();
    }

    this.startIndex = (this.currentPage - 1)*this.pageLimit - (this.firstPage-1)*this.pageLimit;
    this.lastIndex = this.startIndex + this.pageLimit;
  }

  getPlanJson(id){
    this.overlay = true;

    this.Plan.get({ id: id }, (json)=>{
      this.overlay = false;

      var modalInstance = this.uibModal.open({
        templateUrl: 'app/client/modal/modal.html',
        controller: 'ModalCtrl',
        controllerAs: 'mdl',
        size: 'lg',
        resolve: {
          json: function () {
            return json;
          }
        }
      });
    });
  }

  getAgreementJson(id){
    this.overlay = true;

    this.Agreement.get({ id: id }, (json)=>{
      this.overlay = false;
      console.log(json)

      var modalInstance = this.uibModal.open({
        templateUrl: 'app/client/modal/modal.html',
        controller: 'ModalCtrl',
        controllerAs: 'mdl',
        size: 'lg',
        resolve: {
          json: function () {
            return json;
          }
        }
      });
    });
  }

  suspendSubscription(id){
    this.overlay = true;

    this.Agreement.suspend({ id: id }, (res)=>{
      this.overlay = false;
      console.log(res);
    })
  }

  reactivateSubscription(id){
    this.overlay = true;

    this.Agreement.reactivate({ id: id }, (res)=>{
      this.overlay = false;
      console.log(res);
    })
  }

  cancelSubscription(id){
    this.overlay = true;
    
    this.Agreement.cancel({ id: id }, (res)=>{
      this.overlay = false;
      console.log(res);
    })
  }
}

angular.module('owmAdminApp')
  .controller('ClientSubscriptionsCtrl', ClientSubscriptionsController);

})();
