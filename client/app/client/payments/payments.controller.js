'use strict';

(function(){

class ClientPaymentsController{
  constructor($stateParams, $resource) {

    this.Payments = $resource('/api/clients/:id/payments', { id: $stateParams.id });
    this.payments = [];
    this.overlay = false;

    this.totalItems = 0;
    this.lastPage = 0;
    this.firstPage = 0;

    this.currentPage = 1;

    this.pageLimit = 20;
    this.maxSize = 5;
    this.parts = 5;

    this.startIndex = 0;
    this.lastIndex = this.pageLimit;
    this.jsonVisible = true;

    this.getNextPayments();
  }

  getPayments(skip, limit){
    this.overlay = true;
    this.payments = [];

    this.Payments.query({ skip: skip, limit: limit }, (payments)=>{
      this.payments = payments;
      this.totalItems = skip + this.payments.length;
      // Add fake items if not last pages
      this.totalItems += this.payments.length < limit ?  0 : this.pageLimit * this.maxSize;
      this.overlay = false;
    });
  }

  getNextPayments(){
    var skip = this.lastPage * this.pageLimit;
    var limit = this.parts * this.pageLimit * this.maxSize;

    this.getPayments(skip, limit);

    this.firstPage = this.lastPage + 1;
    this.lastPage = this.firstPage - 1 + this.parts * this.maxSize;
  }

  getPrevPayments(){
    var skip = (this.firstPage - 1) * this.pageLimit - this.parts*this.maxSize*this.pageLimit;
    var limit = this.parts * this.pageLimit * this.maxSize;

    this.getPayments(skip, limit);

    this.lastPage = this.firstPage - 1;
    this.firstPage = this.lastPage + 1 - this.parts * this.maxSize;
  }

  pageChanged(){
    if(this.currentPage > this.lastPage){
      this.getNextPayments();
    }
    if(this.currentPage < this.firstPage){
      this.getPrevPayments();
    }

    this.startIndex = (this.currentPage - 1)*this.pageLimit - (this.firstPage-1)*this.pageLimit;
    this.lastIndex = this.startIndex + this.pageLimit;
  }

}

angular.module('owmAdminApp')
  .controller('ClientPaymentsCtrl', ClientPaymentsController);

})();
