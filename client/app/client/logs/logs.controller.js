'use strict';

(function(){

class ClientLogsController{
  constructor($stateParams, $resource) {

    this.Logs = $resource('/api/clients/:id/logs', { id: $stateParams.id });
    this.logs = [];
    this.overlay = false;

    this.totalItems = 0;
    this.lastPage = 0;
    this.firstPage = 0;

    this.currentPage = 1;

    this.pageLimit = 20;
    this.maxSize = 5;
    this.parts = 5;

    this.startIndex = 0;
    this.lastIndex = this.pageLimit;
    this.jsonVisible = true;

    this.getNextLogs();
  }

  getLogs(skip, limit){
    this.overlay = true;
    this.logs = [];

    this.Logs.query({ skip: skip, limit: limit }, (logs)=>{
      this.logs = logs;
      this.totalItems = skip + this.logs.length;
      // Add fake items if not last pages
      this.totalItems += this.logs.length < limit ?  0 : this.pageLimit * this.maxSize;
      this.overlay = false;
    });
  }

  getNextLogs(){
    var skip = this.lastPage * this.pageLimit;
    var limit = this.parts * this.pageLimit * this.maxSize;

    this.getLogs(skip, limit);

    this.firstPage = this.lastPage + 1;
    this.lastPage = this.firstPage - 1 + this.parts * this.maxSize;
  }

  getPrevLogs(){
    var skip = (this.firstPage - 1) * this.pageLimit - this.parts*this.maxSize*this.pageLimit;
    var limit = this.parts * this.pageLimit * this.maxSize;

    this.getLogs(skip, limit);

    this.lastPage = this.firstPage - 1;
    this.firstPage = this.lastPage + 1 - this.parts * this.maxSize;
  }

  pageChanged(){
    if(this.currentPage > this.lastPage){
      this.getNextLogs();
    }
    if(this.currentPage < this.firstPage){
      this.getPrevLogs();
    }

    this.startIndex = (this.currentPage - 1)*this.pageLimit - (this.firstPage-1)*this.pageLimit;
    this.lastIndex = this.startIndex + this.pageLimit;
  }
}

angular.module('owmAdminApp')
  .controller('ClientLogsCtrl', ClientLogsController);

})();
