'use strict';

(function() {

class ModalController {

  constructor($scope, $uibModalInstance, json) {
    this.json = json;

    this.uibModalInstance = $uibModalInstance;
  }

  close(){
    this.uibModalInstance.dismiss('cancel');
  }
}

angular.module('owmAdminApp')
  .controller('ModalCtrl', ModalController);

})();
