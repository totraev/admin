'use strict';

angular.module('owmAdminApp.admin', [
  'owmAdminApp.auth',
  'ui.router'
]);
