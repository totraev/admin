'use strict';

angular.module('owmAdminApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.notification', {
        url: '/notification',
        templateUrl: 'app/notification/notification.html',
        controller: 'NotificationCtrl',
        controllerAs: 'ntf'
      });
  });
