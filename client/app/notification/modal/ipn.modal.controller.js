'use strict';

(function() {

class IpnModalController {

  constructor($scope, $uibModalInstance, ipn_msg) {
    this.ipn_msg = ipn_msg;
    this.uibModalInstance = $uibModalInstance;
  }

  close(){
    this.uibModalInstance.dismiss('cancel');
  }
}

angular.module('owmAdminApp')
  .controller('IpnModalCtrl', IpnModalController);

})();
