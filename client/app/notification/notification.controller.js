'use strict';

(function(){

class NotificationController{

  constructor($resource, $uibModal) {
    this.uibModal = $uibModal;

    this.Notification = $resource('/api/notifications');
    this.notifications = [];
    this.overlay = false;

    this.totalItems = 0;
    this.lastPage = 0;
    this.firstPage = 0;

    this.currentPage = 1;

    this.pageLimit = 50;
    this.maxSize = 10;
    this.parts = 5;

    this.startIndex = 0;
    this.lastIndex = this.pageLimit;
    this.jsonVisible = true;

    this.transactionType = 'all';

    var tomorrow = new Date();
    tomorrow.setUTCDate(tomorrow.getUTCDate());

    this.dtFrom = { opened: false, date: null, options: { showWeeks: false, minDate: null, maxDate: tomorrow } };
    this.dtTo = { opened: false, date: new Date(), options: { showWeeks: false, minDate: null, maxDate: tomorrow} };

    this.getNextNotifications();
  }

  getNotifications(skip, limit){
    this.overlay = true;
    this.notifications = [];
    var query = {};

    if(skip) query.skip = skip;
    if(limit) query.limit = limit;
    if(this.transactionType != 'all') query.transaction = this.transactionType;
    if(this.dtFrom.date) query.dateFrom = this.dtFrom.date.toISOString().slice(0, 10);
    if(this.dtTo.date) query.dateTo = this.dtTo.date.toISOString().slice(0, 10);

    this.Notification.query(query, (notifications)=>{
      this.notifications = notifications;
      if(this.notifications[0])
        this.ipn_msg = this.notifications[0].ipn_msg;
      this.totalItems = skip + this.notifications.length;
      // Add fake items if not last pages
      this.totalItems += this.notifications.length < limit ?  0 : this.pageLimit * this.maxSize;
      this.overlay = false;
    });
  }

  getNextNotifications(){
    var skip = this.lastPage * this.pageLimit;
    var limit = this.parts * this.pageLimit * this.maxSize;

    this.getNotifications(skip, limit);

    this.firstPage = this.lastPage + 1;
    this.lastPage = this.firstPage - 1 + this.parts * this.maxSize;
  }

  getPrevNotifications(){
    var skip = (this.firstPage - 1) * this.pageLimit - this.parts*this.maxSize*this.pageLimit;
    var limit = this.parts * this.pageLimit * this.maxSize;

    this.getNotifications(skip, limit);

    this.lastPage = this.firstPage - 1;
    this.firstPage = this.lastPage + 1 - this.parts * this.maxSize;
  }

  pageChanged(){
    if(this.currentPage > this.lastPage){
      this.getNextNotifications();
    }
    if(this.currentPage < this.firstPage){
      this.getPrevNotifications();
    }

    this.startIndex = (this.currentPage - 1)*this.pageLimit - (this.firstPage-1)*this.pageLimit;
    this.lastIndex = this.startIndex + this.pageLimit;
  }

  search(){
    var limit = this.parts * this.pageLimit * this.maxSize;
    this.getNotifications(0, limit);
  }

  popupFromOpen(){
    this.dtFrom.opened = true;
  }

  popupToOpen(){
    this.dtTo.opened = true;
  }

  popupToChange(){
    this.dtFrom.options.maxDate = this.dtTo.date;
    this.search();
  }

  popupFromChange(){
    this.dtTo.options.minDate = this.dtFrom.date;
    this.search();
  }

  reset(){
    this.transactionType = 'all';
    this.dtFrom.date = null;
    this.dtTo.date = null;

    this.search();
  }

  ipnOpen(msg){
    var modalInstance = this.uibModal.open({
      templateUrl: 'app/notification/modal/ipn.html',
      controller: 'IpnModalCtrl',
      controllerAs: 'ipn',
      size: 'lg',
      resolve: {
        ipn_msg: function () {
          return msg;
        }
      }
    });
  }
}

angular.module('owmAdminApp')
  .controller('NotificationCtrl', NotificationController);

})();
