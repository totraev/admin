'use strict';

angular.module('owmAdminApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.pp_payment', {
        url: '/pp_payment',
        templateUrl: 'app/pp_payment/pp_payment.html',
        controller: 'PpPaymentCtrl',
        controllerAs: 'ppp'
      });
  });
