'use strict';

(function(){

class PpPaymentController{

  constructor($stateParams, $resource){
    this.Payments = $resource('/paypal/payments', {}, { payment: { method: 'GET', url: '/paypal/payments/:id' } });
    this.overlay = false;

    this.prev_ids = [];
    this.next_id = '';
    this.count = '20';
    this.index = 0;

    this.sort_by = 'create_time';
    this.sort_order = 'desc';

    var tomorrow = new Date();
    tomorrow.setUTCDate(tomorrow.getUTCDate());

    this.dtFrom = { opened: false, date: null, options: { showWeeks: false, minDate: null, maxDate: tomorrow } };
    this.dtTo = { opened: false, date: new Date(), options: { showWeeks: false, minDate: null, maxDate: tomorrow} };

    this.query();
  }

  query(){
    this.overlay = true;
    this.payments = [];
    var query = { count: this.count, sort_order: this.sort_order, sort_by: this.sort_by };

    if(this.start_id) query.start_id = this.start_id;
    if(this.dtFrom.date) query.dateFrom = this.dtFrom.date.toISOString().slice(0, 10);
    if(this.dtTo.date) query.dateTo = this.dtTo.date.toISOString().slice(0, 10);

    this.Payments.get(query, (data)=>{
      this.data = data;
      this.payments = data.payments;
      this.overlay = false;
    });
  }

  reset(){
    this.prev_ids = [];
    this.next_id = '';
    this.index = 0;

    this.query();
  }

  next(){
    if(this.data.next_id){
      this.start_id = this.data.next_id;
      this.prev_ids.push(this.data.payments[0].id);

      this.index += parseInt(this.count, 10);
      this.query();
    }
  }

  prev(){
    if(this.prev_ids.length){
      this.start_id = this.prev_ids.pop();

      this.index -= parseInt(this.count, 10);
      this.query();
    }
  }

  startIndex(){
    return this.index + 1;
  }

  popupFromOpen(){
    this.dtFrom.opened = true;
  }

  popupToOpen(){
    this.dtTo.opened = true;
  }

  popupToChange(){
    this.dtFrom.options.maxDate = this.dtTo.date;
    this.query();
  }

  popupFromChange(){
    this.dtTo.options.minDate = this.dtFrom.date;
    this.query();
  }

}

angular.module('owmAdminApp')
  .controller('PpPaymentCtrl', PpPaymentController);

})();
