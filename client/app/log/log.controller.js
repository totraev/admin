'use strict';

(function(){

class LogController{

  constructor($resource, $uibModal) {
    this.uibModal = $uibModal;

    this.Log = $resource('/api/logs');
    this.logs = [];
    this.overlay = false;

    this.totalItems = 0;
    this.lastPage = 0;
    this.firstPage = 0;

    this.currentPage = 1;

    this.pageLimit = 50;
    this.maxSize = 10;
    this.parts = 5;

    this.startIndex = 0;
    this.lastIndex = this.pageLimit;

    this.service = 'all';
    this.action = 'all';
    this.plan = 'all';
    this.plans = ['startup', 'dev', 'pro', 'ent', 'start', 'med', 'adv'];

    this.services = {
      weather: ['startup', 'dev', 'pro', 'ent'],
      history: ['start', 'med', 'adv']
    }

    var tomorrow = new Date();
    tomorrow.setUTCDate(tomorrow.getUTCDate());

    this.dtFrom = { opened: false, date: null, options: { showWeeks: false, minDate: null, maxDate: tomorrow } };
    this.dtTo = { opened: false, date: new Date(), options: { showWeeks: false, minDate: null, maxDate: tomorrow} };

    this.getNextLogs();
  }

  getLogs(skip, limit){
    this.overlay = true;
    this.logs = [];
    var query = {};

    if(skip) query.skip = skip;
    if(limit) query.limit = limit;
    if(this.service != 'all') query.service = this.service;
    if(this.plan != 'all') query.plan = this.plan;
    if(this.action != 'all') query.action = this.action;
    if(this.dtFrom.date) query.dateFrom = this.dtFrom.date.toISOString().slice(0, 10);
    if(this.dtTo.date) query.dateTo = this.dtTo.date.toISOString().slice(0, 10);

    this.Log.query(query, (logs)=>{
      this.logs = logs;

      this.totalItems = skip + this.logs.length;
      // Add fake items if not last pages
      this.totalItems += this.logs.length < limit ?  0 : this.pageLimit * this.maxSize;
      this.overlay = false;
    });
  }

  getNextLogs(){
    var skip = this.lastPage * this.pageLimit;
    var limit = this.parts * this.pageLimit * this.maxSize;

    this.getLogs(skip, limit);

    this.firstPage = this.lastPage + 1;
    this.lastPage = this.firstPage - 1 + this.parts * this.maxSize;
  }

  getPrevLogs(){
    var skip = (this.firstPage - 1) * this.pageLimit - this.parts*this.maxSize*this.pageLimit;
    var limit = this.parts * this.pageLimit * this.maxSize;

    this.getLogs(skip, limit);

    this.lastPage = this.firstPage - 1;
    this.firstPage = this.lastPage + 1 - this.parts * this.maxSize;
  }

  pageChanged(){
    if(this.currentPage > this.lastPage){
      this.getNextLogs();
    }
    if(this.currentPage < this.firstPage){
      this.getPrevLogs();
    }

    this.startIndex = (this.currentPage - 1)*this.pageLimit - (this.firstPage-1)*this.pageLimit;
    this.lastIndex = this.startIndex + this.pageLimit;
  }

  search(){
    var limit = this.parts * this.pageLimit * this.maxSize;
    this.plans = this.service == 'all'? _.concat(this.services.weather, this.services.history): this.services[this.service];

    this.getLogs(0, limit);
  }

  popupFromOpen(){
    this.dtFrom.opened = true;
  }

  popupToOpen(){
    this.dtTo.opened = true;
  }

  popupToChange(){
    this.dtFrom.options.maxDate = this.dtTo.date;
    this.search();
  }

  popupFromChange(){
    this.dtTo.options.minDate = this.dtFrom.date;
    this.search();
  }
}

angular.module('owmAdminApp')
  .controller('LogCtrl', LogController);

})();
