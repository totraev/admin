'use strict';

angular.module('owmAdminApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.log', {
        url: '/log',
        templateUrl: 'app/log/log.html',
        controller: 'LogCtrl',
        controllerAs: 'log'
      });
  });
