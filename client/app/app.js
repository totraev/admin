'use strict';

angular.module('owmAdminApp', [
  'owmAdminApp.auth',
  'owmAdminApp.admin',
  'owmAdminApp.constants',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ui.router',
  'ui.bootstrap',
  'validation.match',
  'ngPrettyJson'
])
  .config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider
      .otherwise('/clients');
    $stateProvider
      .state('account', {
        abstract: true,
        templateUrl: 'app/account/layout.html',
      })
      .state('main', {
        abstract: true,
        templateUrl: 'app/layout/layout.html',
        data: { auth: true }
      });

    $locationProvider.html5Mode(true);
  })
  // allow DI for use in controllers, unit tests
  .constant('_', window._)
  // use in views, ng-repeat="x in _.range(3)"
  .run(function ($rootScope) {
     $rootScope._ = window._;
  });;
