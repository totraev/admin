'use strict';

(function() {

class MainController {

  constructor($resource) {
    this.User = $resource('/api/clients');
    this.users = [];
    this.overlay = false;

    this.totalItems = 0;
    this.lastPage = 0;
    this.firstPage = 0;

    this.currentPage = 1;

    this.pageLimit = 100;
    this.maxSize = 10;
    this.parts = 5;

    this.startIndex = 0;
    this.lastIndex = this.pageLimit;

    this.searchVal = '';
    this.searchBy = 'name';
    this.userType = 'all';

    this.getNextClients();
  }

  getClients(skip, limit){
    this.overlay = true;
    this.users = [];

    var query = {};
    if(skip){
      query.skip = skip;
    }
    if(limit){
      query.limit = limit
    }
    if(this.searchVal){
      query[this.searchBy] = this.searchVal;
    }
    if(this.userType != 'all'){
      query.paypal_user = this.userType == 'billing';
    }

    this.User.query(query, (users)=>{
      this.users = users;
      this.totalItems = skip + this.users.length;
      // Add fake items if not last pages
      this.totalItems += this.users.length < limit ?  0 : this.pageLimit * this.maxSize;
      this.overlay = false;
    });
  }

  getNextClients(){
    var skip = this.lastPage * this.pageLimit;
    var limit = this.parts * this.pageLimit * this.maxSize;

    this.getClients(skip, limit);

    this.firstPage = this.lastPage + 1;
    this.lastPage = this.firstPage - 1 + this.parts * this.maxSize;
  }

  getPrevClients(){
    var skip = (this.firstPage - 1) * this.pageLimit - this.parts*this.maxSize*this.pageLimit;
    var limit = this.parts * this.pageLimit * this.maxSize;

    this.getClients(skip, limit);

    this.lastPage = this.firstPage - 1;
    this.firstPage = this.lastPage + 1 - this.parts * this.maxSize;
  }

  pageChanged(){
    if(this.currentPage > this.lastPage){
      this.getNextClients();
    }
    if(this.currentPage < this.firstPage){
      this.getPrevClients();
    }

    this.startIndex = (this.currentPage - 1)*this.pageLimit - (this.firstPage-1)*this.pageLimit;
    this.lastIndex = this.startIndex + this.pageLimit;
  }

  search(){
    this.getClients(0, 5000);
  }
}

angular.module('owmAdminApp')
  .controller('MainController', MainController);

})();
