'use strict';

angular.module('owmAdminApp')
  .directive('contentHeight', ['$window', function ($window) {
    return {
      restrict: 'A',
      link: function (scope, element) {
        var height = $window.innerHeight - 50 - 51; //height without footer & header
        if(height > element.height()){
          element.css('min-height', height);
        }
      }
    };
  }]);
