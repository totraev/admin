'use strict';

angular.module('owmAdminApp')
  .config(function($stateProvider) {
    $stateProvider
      .state('main.clients', {
        url: '/clients',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      });
  });
