'use strict';

angular.module('owmAdminApp')
  .filter('pagination', function () {
    return function (input, start, end) {      
      return input.slice(start, end);
    };
  });
