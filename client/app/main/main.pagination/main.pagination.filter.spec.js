'use strict';

describe('Filter: main.pagination', function () {

  // load the filter's module
  beforeEach(module('owmAdminApp'));

  // initialize a new instance of the filter before each test
  var main.pagination;
  beforeEach(inject(function ($filter) {
    main.pagination = $filter('main.pagination');
  }));

  it('should return the input prefixed with "main.pagination filter:"', function () {
    var text = 'angularjs';
    expect(main.pagination(text)).to.equal('main.pagination filter: ' + text);
  });

});
