'use strict';

angular.module('owmAdminApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.plan', {
        url: '/plan',
        templateUrl: 'app/plan/plan.html',
        controller: 'PlanCtrl',
        controllerAs: 'pln'
      });
  });
