'use strict';

(function(){

class PlanController{

  constructor($resource, $uibModal) {
    this.uibModal = $uibModal;

    this.Plans = $resource('/paypal/plans');
    this.Plan = $resource('/paypal/plans/:id');

    this.plans = {};
    this.totalPages = 0;
    this.totalItems = 0;
    this.currentPage = 1;
    this.status = 'ACTIVE';

    this.request();
  }

  request(){
    this.plans = [];
    this.overlay = true;

    this.Plans.get({status: this.status, page: this.currentPage - 1}, (json)=>{
      console.log(json);
      this.plans = json.plans;
      this.totalItems = parseInt(json.total_items, 10);
      this.totalPages = parseInt(json.total_pages, 10);
      this.overlay = false;
    });
  }

  pageChanged(){
    this.request();
  }

  openPlanModal(planId){
    this.overlay = true;
    this.Plan.get({ id: planId }, (plan)=>{
      this.overlay = false;

      var modalInstance = this.uibModal.open({
        templateUrl: 'app/plan/modal/planModal.html',
        controller: 'PlanModalCtrl',
        controllerAs: 'pm',
        size: 'lg',
        resolve: {
          plan: function () {
            return plan;
          }
        }
      });
    });
  }
}

angular.module('owmAdminApp')
  .controller('PlanCtrl', PlanController);

})();
