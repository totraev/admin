'use strict';

(function() {

class PlanModalController {

  constructor($scope, $uibModalInstance, plan) {
    this.plan = plan;

    this.uibModalInstance = $uibModalInstance;
  }

  close(){
    this.uibModalInstance.dismiss('cancel');
  }
}

angular.module('owmAdminApp')
  .controller('PlanModalCtrl', PlanModalController);

})();
