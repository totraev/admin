(function(angular, undefined) {
'use strict';

angular.module('owmAdminApp.constants', [])

.constant('appConfig', {userRoles:['admin']})

;
})(angular);