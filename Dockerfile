FROM node:argon

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

ADD . /usr/src/app

RUN npm install -g bower grunt-cli
RUN npm install && bower install --allow-root

EXPOSE 9000

RUN grunt build

CMD ["npm", "start"]
